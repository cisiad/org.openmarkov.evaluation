/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.evaluation.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openmarkov.core.model.network.Variable;

public class EvaluationResult
{
    private List<Variable> classVariables = null;
    private List<Variable> evidenceVariables = null;
    private Map<Variable, Double> successRates = null;
    private ArrayList<EvaluationSample> samples      = null;
    private boolean mustRecalculate = true;

    /**
     * Constructor for EvaluationResult.
     * @param classVariables
     * @param evidenceVariables
     */
    public EvaluationResult (List<Variable> classVariables, List<Variable> evidenceVariables)
    {
        this.classVariables = classVariables;
        this.evidenceVariables = evidenceVariables;
        this.samples = new ArrayList<EvaluationSample> ();
        this.successRates = new HashMap<Variable, Double> ();
    }

    /**
     * Adds a sample to the evaluation result sample list
     * @param sample
     */
    public void addSample(EvaluationSample sample)
    {
        this.samples.add (sample);
        this.mustRecalculate = true;
    }

    /**
     * Returns the successRates.
     * @return the successRates.
     */
    public Map<Variable, Double> getSuccessRates ()
    {
        if(mustRecalculate)
        {
            for(Variable classVariable: classVariables)
            {
                successRates.put (classVariable, 0.0);
            }            
            for(EvaluationSample sample: samples)
            {
                for(Variable classVariable : classVariables)
                {
                    if(sample.getClassVariables ().get(classVariable.getName ()).isSuccess ())
                    {
                        successRates.put (classVariable, successRates.get (classVariable) + 1);
                    }
                }
            }
            for(Variable classVariable: classVariables)
            {
                successRates.put (classVariable, successRates.get (classVariable)/(double)samples.size () );
            }
            mustRecalculate = false;
        }
        return successRates;
    }

    /**
     * Returns the samples.
     * @return the samples.
     */
    public List<EvaluationSample> getSamples ()
    {
        return samples;
    }

    /**
     * Returns the classVariables.
     * @return the classVariables.
     */
    public List<Variable> getClassVariables ()
    {
        return classVariables;
    }

    /**
     * Returns the evidenceVariables.
     * @return the evidenceVariables.
     */
    public List<Variable> getEvidenceVariables ()
    {
        return evidenceVariables;
    }   
}
