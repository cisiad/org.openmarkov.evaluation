/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.evaluation.result;

import java.util.HashMap;

import org.openmarkov.core.model.network.Variable;

public class EvaluationSample
{
    private HashMap<String, String>                  evidenceVariables = null;
    private HashMap<String, EvaluationClassVariable> classVariables    = null;

    /**
     * Constructor for Sample.
     * @param evidenceVariables
     * @param classVariables
     */
    public EvaluationSample (HashMap<String, String> evidenceVariables,
                             HashMap<String, EvaluationClassVariable> classVariables)
    {
        this.evidenceVariables = evidenceVariables;
        this.classVariables = classVariables;
    }

    /**
     * Constructor for Sample.
     */
    public EvaluationSample ()
    {
        this.evidenceVariables = new HashMap<String, String> ();
        this.classVariables = new HashMap<String, EvaluationClassVariable> ();
    }

    /**
     * Add an evidence variable to the sample
     * @param variable
     * @param state
     */
    public void addEvidenceVariable (Variable variable, int state)
    {
        this.evidenceVariables.put (variable.getName (), variable.getStateName (state));
    }

    /**
     * 
     * @param classVariable
     * @param inferredProbabilities
     * @param mostProbableState
     * @param actualState
     */
    public void addClassVariable (Variable classVariable,
                                  double[] inferredProbabilities,
                                  int mostProbableState,
                                  int actualState)
    {
        this.classVariables.put (classVariable.getName (),
                                 new EvaluationClassVariable (
                                                              inferredProbabilities,
                                                              classVariable.getStateName (actualState),
                                                              classVariable.getStateName (mostProbableState)));        
    }

    /**
     * Returns the evidenceVariables.
     * @return the evidenceVariables.
     */
    public HashMap<String, String> getEvidenceVariables ()
    {
        return evidenceVariables;
    }

    /**
     * Returns the classVariables.
     * @return the classVariables.
     */
    public HashMap<String, EvaluationClassVariable> getClassVariables ()
    {
        return classVariables;
    }


}