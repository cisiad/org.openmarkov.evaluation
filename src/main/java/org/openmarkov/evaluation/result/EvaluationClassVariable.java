/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */
package org.openmarkov.evaluation.result;


public class EvaluationClassVariable
{
    private double[]                statesProbabilities = null;
    private String                  originalState       = null;
    private String                  chosenState         = null;
    private boolean isSuccess = false;

    /**
     * Constructor for ClassVariable.
     * @param statesProbabilities
     * @param originalState
     * @param chosenState
     */
    public EvaluationClassVariable (double[] statesProbabilities,
                          String originalState,
                          String chosenState)
    {
        this.statesProbabilities = statesProbabilities;
        this.originalState = originalState;
        this.chosenState = chosenState;
        this.isSuccess = this.originalState == this.chosenState; 
    }

    /**
     * Returns the statesProbabilities.
     * @return the statesProbabilities.
     */
    public double[] getStatesProbabilities ()
    {
        return statesProbabilities;
    }

    /**
     * Returns the originalState.
     * @return the originalState.
     */
    public String getOriginalState ()
    {
        return originalState;
    }

    /**
     * Returns the chosenState.
     * @return the chosenState.
     */
    public String getChosenState ()
    {
        return chosenState;
    }
    
    /**
     * Returns whether the class variable was successfully guessed 
     * @return isSuccess
     */
    public boolean isSuccess()
    {
        return this.isSuccess;
    }
}
