package org.openmarkov.evaluation.gui.graphic;

import org.openmarkov.core.gui.graphic.FSVariableBox;
import org.openmarkov.core.gui.graphic.VisualNode;
import org.openmarkov.core.gui.graphic.VisualState;

public class EvaluationInnerBox extends FSVariableBox
{
    public EvaluationInnerBox (VisualNode vNode)
    {
        super (vNode);
    }

    /**
     * This method creates a visual state for each state of the variable.
     * 
     * @param numValues
     *            Number of values that has to be each visual state.
     */
    protected void createVisualStates() {
        this.visualStates.put (0, new VisualState (visualNode, 0, "success"));
        this.visualStates.put (1, new VisualState (visualNode, 1, "failure"));
    }

}
