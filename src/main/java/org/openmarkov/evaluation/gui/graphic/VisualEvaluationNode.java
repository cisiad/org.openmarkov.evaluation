/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */
/*
 * EvaluationGUI.java Created on 5 de junio de 2008, 19:59
 */

package org.openmarkov.evaluation.gui.graphic;

import org.openmarkov.core.gui.graphic.VisualChanceNode;
import org.openmarkov.core.gui.graphic.VisualNetwork;
import org.openmarkov.core.model.network.Node;

public class VisualEvaluationNode extends VisualChanceNode
{
    public VisualEvaluationNode (Node node, VisualNetwork visualNetwork)
    {
        super (node, visualNetwork);
        this.innerBox = new EvaluationInnerBox (this);
    }

    public void setSuccessRate (double successRate)
    {
        ((EvaluationInnerBox) innerBox).getVisualState ("success").setStateValue (0, successRate);
        ((EvaluationInnerBox) innerBox).getVisualState ("failure").setStateValue (0, 1.0 - successRate);
    }
}
