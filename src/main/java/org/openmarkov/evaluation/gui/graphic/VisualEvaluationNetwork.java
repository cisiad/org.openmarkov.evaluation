package org.openmarkov.evaluation.gui.graphic;

import org.openmarkov.core.gui.graphic.VisualNetwork;
import org.openmarkov.core.gui.graphic.VisualNode;
import org.openmarkov.core.gui.window.edition.EditorPanel;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.ProbNet;

public class VisualEvaluationNetwork extends VisualNetwork
{

    public VisualEvaluationNetwork (ProbNet probNet, EditorPanel editorPanel)
    {
        super (probNet);
    }

    @Override
    protected VisualNode createVisualNode (Node node)
    {
        return new VisualEvaluationNode(node, this);
    }
    
}
