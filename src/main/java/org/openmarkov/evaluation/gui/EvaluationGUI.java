/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.evaluation.gui;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.io.FilenameUtils;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.gui.configuration.OpenMarkovPreferences;
import org.openmarkov.core.gui.dialog.io.DBReaderFileChooser;
import org.openmarkov.core.gui.dialog.io.FileFilterAll;
import org.openmarkov.core.gui.dialog.io.NetsIO;
import org.openmarkov.core.gui.dialog.io.NetworkFileChooser;
import org.openmarkov.core.gui.graphic.VisualNode;
import org.openmarkov.core.gui.loader.element.OpenMarkovLogoIcon;
import org.openmarkov.core.gui.localize.StringDatabase;
import org.openmarkov.core.gui.plugin.ToolPlugin;
import org.openmarkov.core.gui.window.MainPanel;
import org.openmarkov.core.gui.window.edition.NetworkPanel;
import org.openmarkov.core.inference.annotation.InferenceManager;
import org.openmarkov.core.io.database.CaseDatabase;
import org.openmarkov.core.io.database.CaseDatabaseReader;
import org.openmarkov.core.io.database.plugin.CaseDatabaseManager;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.evaluation.EvaluationManager;
import org.openmarkov.evaluation.gui.graphic.VisualEvaluationNetwork;
import org.openmarkov.evaluation.gui.graphic.VisualEvaluationNode;
import org.openmarkov.evaluation.io.EvaluationReportGenerator;
import org.openmarkov.evaluation.result.EvaluationResult;

/**
 * GUI to the evaluation option
 * @author fjdiez
 * @author ibermejo
 * @version 1.0
 * @since OpenMarkov 1.0
 */
@SuppressWarnings("serial")
@ToolPlugin(name = "Evaluation", command = "Tools.Evaluation")
public class EvaluationGUI extends javax.swing.JDialog
{
    private ProbNet          net;
    private CaseDatabase     caseDatabase      = null;
    private String           sourcePath        = null;
    private String           netFilePath       = null;
    private String           fileName          = null;
    /**
     * Messages string resource.
     */
    private JFrame           parent;
    private Vector<String>   hiddenVariables   = null;
    private Vector<String>   evidenceVariables = null;
    private Vector<String>   classVariables    = null;
    /**
     * String database
     */
    protected StringDatabase stringDatabase    = StringDatabase.getUniqueInstance ();

    /**
     * Constructor for EvaluationGUI.
     * @param parent
     */
    public EvaluationGUI (JFrame parent)
    {
        super (parent, true);
        this.parent = parent;
        try
        {
            UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
            initComponents ();
            setIconImage (OpenMarkovLogoIcon.getUniqueInstance ().getOpenMarkovLogoIconImage16 ());
        }
        catch (ClassNotFoundException ex)
        {
            Logger.getLogger (EvaluationGUI.class.getName ()).log (Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            Logger.getLogger (EvaluationGUI.class.getName ()).log (Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            Logger.getLogger (EvaluationGUI.class.getName ()).log (Level.SEVERE, null, ex);
        }
        catch (UnsupportedLookAndFeelException ex)
        {
            Logger.getLogger (EvaluationGUI.class.getName ()).log (Level.SEVERE, null, ex);
        }
        setDefaultCloseOperation (HIDE_ON_CLOSE);
        setLocationRelativeTo (null);
        netButtonGroup = new ButtonGroup ();
        netButtonGroup.add (fromFileRadioButton);
        netButtonGroup.add (fromOpenMarkovRadioButton);
        boolean isOpenNet = MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().getCurrentNetworkPanel () != null;
        fromOpenMarkovRadioButton.setEnabled (isOpenNet);
        fromOpenMarkovRadioButton.setSelected (isOpenNet);
        fromFileRadioButton.setSelected (!isOpenNet);
        loadModelNetButton.setEnabled (!isOpenNet);
        hiddenVariables = new Vector<String> ();
        hiddenVariableList.setListData (hiddenVariables);
        evidenceVariables = new Vector<String> ();
        evidenceVariableList.setListData (evidenceVariables);
        classVariables = new Vector<String> ();
        classVariableList.setListData (classVariables);
        setVisible (true);
    }

    private ProbNet loadNet (String filePath)
    {
        ProbNet probNet = null;
        if ((netFilePath != null) && (!netFilePath.equals ("")))
        {
            if (!isSupportedNetFormat (fileName))
            {
                JOptionPane.showMessageDialog (null,
                                               stringDatabase.getString ("Evaluation.IncorrectFileFormat"),
                                               stringDatabase.getString ("ErrorWindow.Title.Label"),
                                               JOptionPane.ERROR_MESSAGE);
            }
            else
            {
                try
                {
                    probNet = NetsIO.openNetworkFile (filePath).getProbNet ();
                }
                catch (Exception e)
                {
                    JOptionPane.showMessageDialog (null,
                                                   stringDatabase.getString ("Evaluation.UnableToLoadNet"),
                                                   stringDatabase.getString ("ErrorWindow.Title.Label"),
                                                   JOptionPane.ERROR_MESSAGE);
                    e.printStackTrace ();
                }
            }
        }
        return probNet;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents ()
    {
        dbFileChooser = new DBReaderFileChooser ();
        netFileChooser = new NetworkFileChooser ();
        netButtonGroup = new javax.swing.ButtonGroup ();
        jPanel5 = new javax.swing.JPanel ();
        evaluateButton = new javax.swing.JButton ();
        cancelButton = new javax.swing.JButton ();
        jPanel8 = new javax.swing.JPanel ();
        fromFileRadioButton = new javax.swing.JRadioButton ();
        fromOpenMarkovRadioButton = new javax.swing.JRadioButton ();
        jScrollPane5 = new javax.swing.JScrollPane ();
        netFilePathTextPane = new javax.swing.JTextPane ();
        loadModelNetButton = new javax.swing.JButton ();
        jPanel1 = new javax.swing.JPanel ();
        jScrollPane2 = new javax.swing.JScrollPane ();
        caseFileTextPane = new javax.swing.JTextPane ();
        loadCaseFileButton = new javax.swing.JButton ();
        jLabel1 = new javax.swing.JLabel ();
        jPanel2 = new javax.swing.JPanel ();
        evidenceVarsScrollPane = new javax.swing.JScrollPane ();
        evidenceVariableList = new javax.swing.JList<String> ();
        hiddenVarsScrollPane = new javax.swing.JScrollPane ();
        hiddenVariableList = new javax.swing.JList<String> ();
        classVarScrollPane = new javax.swing.JScrollPane ();
        classVariableList = new javax.swing.JList<String> ();
        unToEvBtn = new javax.swing.JButton ();
        clToUnBtn = new javax.swing.JButton ();
        evToUnBtn = new javax.swing.JButton ();
        unToClBtn = new javax.swing.JButton ();
        jLabel2 = new javax.swing.JLabel ();
        jLabel3 = new javax.swing.JLabel ();
        jLabel4 = new javax.swing.JLabel ();
        jPanel3 = new javax.swing.JPanel ();
        chkGenerateReport = new javax.swing.JCheckBox ();
        netFileChooser.setCursor (new java.awt.Cursor (java.awt.Cursor.DEFAULT_CURSOR));
        setDefaultCloseOperation (javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle ("OpenMarkov - Evaluation");
        evaluateButton.setText ("Evaluate");
        evaluateButton.setEnabled (false);
        evaluateButton.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    try
                    {
                        evaluateButtonActionPerformed (evt);
                    }
                    catch (NotEvaluableNetworkException e)
                    {
                        JOptionPane.showMessageDialog (null,
                                                       "Error during evaluation. Check message window for details",
                                                       "Error", JOptionPane.ERROR_MESSAGE);
                        e.printStackTrace ();
                    }
                }
            });
        cancelButton.setText ("Cancel");
        cancelButton.setPreferredSize (new java.awt.Dimension (99, 23));
        cancelButton.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    cancelButtonActionPerformed (evt);
                }
            });
        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout (
                                                                                             jPanel5);
        jPanel5.setLayout (jPanel5Layout);
        jPanel5Layout.setHorizontalGroup (jPanel5Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (org.jdesktop.layout.GroupLayout.TRAILING,
                                                                                                                           jPanel5Layout.createSequentialGroup ().addContainerGap (207,
                                                                                                                                                                                   Short.MAX_VALUE).add (evaluateButton).add (28,
                                                                                                                                                                                                                              28,
                                                                                                                                                                                                                              28).add (cancelButton,
                                                                                                                                                                                                                                       org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                       90,
                                                                                                                                                                                                                                       org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (189,
                                                                                                                                                                                                                                                                                            189,
                                                                                                                                                                                                                                                                                            189)));
        jPanel5Layout.linkSize (new java.awt.Component[] {cancelButton, evaluateButton},
                                org.jdesktop.layout.GroupLayout.HORIZONTAL);
        jPanel5Layout.setVerticalGroup (jPanel5Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel5Layout.createSequentialGroup ().addContainerGap (org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                 Short.MAX_VALUE).add (jPanel5Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.BASELINE).add (cancelButton,
                                                                                                                                                                                                                                                                                         org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                         org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                         org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (evaluateButton))));
        jPanel8.setBorder (javax.swing.BorderFactory.createTitledBorder ("Choose net"));
        fromFileRadioButton.setText ("Open from file");
        fromFileRadioButton.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    fromFileRadioButtonActionPerformed (evt);
                }
            });
        fromOpenMarkovRadioButton.setText ("Use open net");
        fromOpenMarkovRadioButton.setActionCommand ("Use open net");
        fromOpenMarkovRadioButton.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    fromOpenMarkovRadioButtonActionPerformed (evt);
                }
            });
        netFilePathTextPane.setEditable (false);
        netFilePathTextPane.setEnabled (false);
        jScrollPane5.setViewportView (netFilePathTextPane);
        loadModelNetButton.setText ("Open");
        loadModelNetButton.setEnabled (false);
        loadModelNetButton.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    loadModelNetButtonActionPerformed (evt);
                }
            });
        org.jdesktop.layout.GroupLayout jPanel8Layout = new org.jdesktop.layout.GroupLayout (
                                                                                             jPanel8);
        jPanel8.setLayout (jPanel8Layout);
        jPanel8Layout.setHorizontalGroup (jPanel8Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel8Layout.createSequentialGroup ().addContainerGap ().add (jPanel8Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel8Layout.createSequentialGroup ().add (fromFileRadioButton).add (18,
                                                                                                                                                                                                                                                                                                                                                 18,
                                                                                                                                                                                                                                                                                                                                                 18).add (loadModelNetButton,
                                                                                                                                                                                                                                                                                                                                                          org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                          88,
                                                                                                                                                                                                                                                                                                                                                          org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap (org.jdesktop.layout.LayoutStyle.UNRELATED).add (jScrollPane5).add (10,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              10,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              10)).add (jPanel8Layout.createSequentialGroup ().add (fromOpenMarkovRadioButton).addContainerGap ()))));
        jPanel8Layout.setVerticalGroup (jPanel8Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel8Layout.createSequentialGroup ().addContainerGap ().add (fromOpenMarkovRadioButton).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED).add (jPanel8Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jScrollPane5,
                                                                                                                                                                                                                                                                                                                                                                   org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                   22,
                                                                                                                                                                                                                                                                                                                                                                   org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (jPanel8Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.BASELINE).add (fromFileRadioButton).add (loadModelNetButton))).addContainerGap (org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           Short.MAX_VALUE)));
        fromOpenMarkovRadioButton.getAccessibleContext ().setAccessibleName ("Use open net");
        jPanel1.setBorder (javax.swing.BorderFactory.createTitledBorder ("Database"));
        caseFileTextPane.setEditable (false);
        caseFileTextPane.setEnabled (false);
        jScrollPane2.setViewportView (caseFileTextPane);
        loadCaseFileButton.setText ("Open");
        loadCaseFileButton.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    loadCaseFileButtonActionPerformed (evt);
                }
            });
        jLabel1.setText ("Database:");
        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout (
                                                                                             jPanel1);
        jPanel1.setLayout (jPanel1Layout);
        jPanel1Layout.setHorizontalGroup (jPanel1Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel1Layout.createSequentialGroup ().addContainerGap ().add (jLabel1,
                                                                                                                                                                                          org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                          org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                          Short.MAX_VALUE).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED).add (jPanel1Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (loadCaseFileButton,
                                                                                                                                                                                                                                                                                                                                                           org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                           90,
                                                                                                                                                                                                                                                                                                                                                           org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (jScrollPane2,
                                                                                                                                                                                                                                                                                                                                                                                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                278,
                                                                                                                                                                                                                                                                                                                                                                                                                org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addContainerGap (org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Short.MAX_VALUE)));
        jPanel1Layout.setVerticalGroup (jPanel1Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel1Layout.createSequentialGroup ().addContainerGap ().add (jPanel1Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jLabel1).add (jPanel1Layout.createSequentialGroup ().add (jScrollPane2,
                                                                                                                                                                                                                                                                                                                                   org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                   org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                   org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED).add (loadCaseFileButton))).addContainerGap (org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         Short.MAX_VALUE)));
        jPanel2.setBorder (javax.swing.BorderFactory.createTitledBorder ("Variables"));
        evidenceVariableList.setModel (new javax.swing.AbstractListModel<String> ()
            {
                String[] strings = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"};

                public int getSize ()
                {
                    return strings.length;
                }

                public String getElementAt (int i)
                {
                    return strings[i];
                }
            });
        evidenceVarsScrollPane.setViewportView (evidenceVariableList);
        hiddenVariableList.setModel (new javax.swing.AbstractListModel<String> ()
            {
                String[] strings = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"};

                public int getSize ()
                {
                    return strings.length;
                }

                public String getElementAt (int i)
                {
                    return strings[i];
                }
            });
        hiddenVarsScrollPane.setViewportView (hiddenVariableList);
        classVariableList.setModel (new javax.swing.AbstractListModel<String> ()
            {
                String[] strings = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"};

                public int getSize ()
                {
                    return strings.length;
                }

                public String getElementAt (int i)
                {
                    return strings[i];
                }
            });
        classVarScrollPane.setViewportView (classVariableList);
        unToEvBtn.setText ("<-");
        unToEvBtn.setEnabled (false);
        unToEvBtn.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    unToEvBtnActionPerformed (evt);
                }
            });
        clToUnBtn.setText ("<-");
        clToUnBtn.setEnabled (false);
        clToUnBtn.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    clToUnBtnActionPerformed (evt);
                }
            });
        evToUnBtn.setText ("->");
        evToUnBtn.setEnabled (false);
        evToUnBtn.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    evToUnBtnActionPerformed (evt);
                }
            });
        unToClBtn.setText ("->");
        unToClBtn.setEnabled (false);
        unToClBtn.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    unToClBtnActionPerformed (evt);
                }
            });
        jLabel2.setText ("Evidence Variables:");
        jLabel3.setText ("Hidden Variables:");
        jLabel4.setText ("Class Variables:");
        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout (
                                                                                             jPanel2);
        jPanel2.setLayout (jPanel2Layout);
        jPanel2Layout.setHorizontalGroup (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel2Layout.createSequentialGroup ().add (18,
                                                                                                                                                                       18,
                                                                                                                                                                       18).add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel2Layout.createSequentialGroup ().add (evidenceVarsScrollPane,
                                                                                                                                                                                                                                                                                                             org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                             138,
                                                                                                                                                                                                                                                                                                             org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED,
                                                                                                                                                                                                                                                                                                                                                                              20,
                                                                                                                                                                                                                                                                                                                                                                              Short.MAX_VALUE).add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (unToEvBtn,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     44,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (evToUnBtn,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          44,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).add (18,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                18,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                18)).add (jPanel2Layout.createSequentialGroup ().add (jLabel2).add (123,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    123,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    123))).add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel2Layout.createSequentialGroup ().add (hiddenVarsScrollPane,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             138,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (18,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  18,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  18).add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (clToUnBtn,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            44,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (unToClBtn,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 44,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))).add (jLabel3)).add (16,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       16,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       16).add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   false).add (jLabel4).add (classVarScrollPane,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             138,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addContainerGap ()));
        jPanel2Layout.linkSize (new java.awt.Component[] {classVarScrollPane,
                                        evidenceVarsScrollPane, hiddenVarsScrollPane},
                                org.jdesktop.layout.GroupLayout.HORIZONTAL);
        jPanel2Layout.setVerticalGroup (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel2Layout.createSequentialGroup ().addContainerGap ().add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.BASELINE).add (jLabel2).add (jLabel3).add (jLabel4)).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED).add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (org.jdesktop.layout.GroupLayout.TRAILING,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                classVarScrollPane,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                144,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                Short.MAX_VALUE).add (hiddenVarsScrollPane).add (org.jdesktop.layout.GroupLayout.TRAILING,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 evidenceVarsScrollPane).add (jPanel2Layout.createSequentialGroup ().add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (unToEvBtn).add (clToUnBtn)).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED).add (jPanel2Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (evToUnBtn).add (unToClBtn)).add (0,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        0,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        Short.MAX_VALUE)))));
        chkGenerateReport.setText ("Generate Report");
        chkGenerateReport.addActionListener (new java.awt.event.ActionListener ()
            {
                public void actionPerformed (java.awt.event.ActionEvent evt)
                {
                    chkGenerateReportActionPerformed (evt);
                }
            });
        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout (
                                                                                             jPanel3);
        jPanel3.setLayout (jPanel3Layout);
        jPanel3Layout.setHorizontalGroup (jPanel3Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (jPanel3Layout.createSequentialGroup ().add (21,
                                                                                                                                                                       21,
                                                                                                                                                                       21).add (chkGenerateReport).addContainerGap (org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                    Short.MAX_VALUE)));
        jPanel3Layout.setVerticalGroup (jPanel3Layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (org.jdesktop.layout.GroupLayout.TRAILING,
                                                                                                                         jPanel3Layout.createSequentialGroup ().addContainerGap (org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                 Short.MAX_VALUE).add (chkGenerateReport).addContainerGap ()));
        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout (
                                                                                      getContentPane ());
        getContentPane ().setLayout (layout);
        layout.setHorizontalGroup (layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (layout.createSequentialGroup ().addContainerGap ().add (layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (org.jdesktop.layout.GroupLayout.TRAILING,
                                                                                                                                                                                                                                               jPanel1,
                                                                                                                                                                                                                                               org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                               org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                               Short.MAX_VALUE).add (org.jdesktop.layout.GroupLayout.TRAILING,
                                                                                                                                                                                                                                                                     jPanel8,
                                                                                                                                                                                                                                                                     org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                     org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                     Short.MAX_VALUE).add (jPanel2,
                                                                                                                                                                                                                                                                                           org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                           org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                           Short.MAX_VALUE).add (org.jdesktop.layout.GroupLayout.TRAILING,
                                                                                                                                                                                                                                                                                                                 layout.createSequentialGroup ().add (layout.createParallelGroup (org.jdesktop.layout.GroupLayout.TRAILING).add (org.jdesktop.layout.GroupLayout.LEADING,
                                                                                                                                                                                                                                                                                                                                                                                                                                 jPanel3,
                                                                                                                                                                                                                                                                                                                                                                                                                                 org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                 org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                 Short.MAX_VALUE).add (jPanel5,
                                                                                                                                                                                                                                                                                                                                                                                                                                                       org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                       org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                       Short.MAX_VALUE)).addContainerGap ()))));
        layout.setVerticalGroup (layout.createParallelGroup (org.jdesktop.layout.GroupLayout.LEADING).add (layout.createSequentialGroup ().add (21,
                                                                                                                                                21,
                                                                                                                                                21).add (jPanel1,
                                                                                                                                                         org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                         org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                         org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED).add (jPanel8,
                                                                                                                                                                                                                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap (org.jdesktop.layout.LayoutStyle.UNRELATED).add (jPanel2,
                                                                                                                                                                                                                                                                                                                                                                                         org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                         org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                         org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED).add (jPanel3,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap (org.jdesktop.layout.LayoutStyle.RELATED,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         Short.MAX_VALUE).add (jPanel5,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add (25,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    25,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    25)));
        jPanel8.getAccessibleContext ().setAccessibleName ("Select net");
        jPanel1.getAccessibleContext ().setAccessibleName ("Database");
        pack ();
    }// </editor-fold>//GEN-END:initComponents

    private void chkGenerateReportActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_chkGenerateReportActionPerformed
        // Do nothing
    }// GEN-LAST:event_chkGenerateReportActionPerformed

    private void loadDatabase ()
    {
        String path = dbFileChooser.getSelectedFile ().getAbsolutePath ();
        if ((path != null) && (!path.equals ("")))
        {
            CaseDatabaseManager caseDbManager = new CaseDatabaseManager ();
            CaseDatabaseReader reader = caseDbManager.getReader (FilenameUtils.getExtension (path));
            if (reader == null)
            {
                JOptionPane.showMessageDialog (null,
                                               stringDatabase.getString ("Evaluation.IncorrectCaseDatabaseFileFormat"),
                                               stringDatabase.getString ("ErrorWindow.Title.Label"),
                                               JOptionPane.ERROR_MESSAGE);
                dbFileChooser.setSelectedFile (null);
                caseFileTextPane.setText (null);
                evaluateButton.setEnabled (false);
            }
            else
            {
                sourcePath = dbFileChooser.getSelectedFile ().getAbsolutePath ();
                try
                {
                    // Load the database
                    if (sourcePath != null)
                    {
                        caseDatabase = reader.load (sourcePath);
                        evaluateButton.setEnabled (true);
                        netFileChooser.setSelectedFile (null);
                        net = null;
                    }
                }
                catch (IOException ex)
                {
                    Logger.getLogger (EvaluationGUI.class.getName ()).log (Level.SEVERE, null, ex);
                }
            }
        }
        else evaluateButton.setEnabled (false);
    }

    /**
     * It asks the user to choose a file by means of a open-file dialog box.
     * @return complete path of the file, or null if the user selects cancel.
     */
    private String requestNetworkFileToOpen ()
    {
        netFileChooser.setDialogTitle (stringDatabase.getString ("OpenNetwork.Title.Label"));
        File currentDirectory = new File (
                                          OpenMarkovPreferences.get (OpenMarkovPreferences.LAST_OPEN_DIRECTORY,
                                                                     OpenMarkovPreferences.OPENMARKOV_DIRECTORIES,
                                                                     "."));
        netFileChooser.setCurrentDirectory (currentDirectory);
        String filePath = (netFileChooser.showOpenDialog (this.parent) == JFileChooser.APPROVE_OPTION) ? netFileChooser.getSelectedFile ().getAbsolutePath ()
                                                                                                      : null;
        if (filePath != null)
        {
            fileName = netFileChooser.getSelectedFile ().getName ();
            netFilePathTextPane.setText (filePath);
        }
        return filePath;
    }

    private void cancelButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible (false);
    }// GEN-LAST:event_cancelButtonActionPerformed

    private void evaluateButtonActionPerformed (java.awt.event.ActionEvent evt)
        throws NotEvaluableNetworkException
    {// GEN-FIRST:event_EvaluateButtonActionPerformed
        ArrayList<String> variableNames = new ArrayList<String> ();
        for (Variable variable : caseDatabase.getVariables ())
        {
            variableNames.add (variable.getName ());
        }
        // Check that we have at least one class variable
        if (classVariables.isEmpty ())
        {
            JOptionPane.showMessageDialog (null,
                                           stringDatabase.getString ("Evaluation.NoClassVariable"),
                                           stringDatabase.getString ("ErrorWindow.Title.Label"),
                                           JOptionPane.ERROR_MESSAGE);
        }// Check if all class variables are in the database
        else if (!variableNames.containsAll (classVariables))
        {
            JOptionPane.showMessageDialog (null,
                                           stringDatabase.getString ("Evaluation.ClassVariablesNotInDB"),
                                           stringDatabase.getString ("ErrorWindow.Title.Label"),
                                           JOptionPane.ERROR_MESSAGE);
        }// Check if all evidence variables are in the database
        else if (!variableNames.containsAll (evidenceVariables))
        {
            JOptionPane.showMessageDialog (null,
                                           stringDatabase.getString ("Evaluation.EvidenceVariablesNotInDB"),
                                           stringDatabase.getString ("ErrorWindow.Title.Label"),
                                           JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            InferenceManager inferenceManager = new InferenceManager ();
            EvaluationManager evaluationManager = new EvaluationManager ();
            EvaluationResult evaluationResult = evaluationManager.evaluate (net,
                                                                            caseDatabase,
                                                                            classVariables,
                                                                            evidenceVariables,
                                                                            inferenceManager.getDefaultInferenceAlgorithm (net));
            net.setName ("Evaluation");
            NetworkPanel evaluationPanel = MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().createNewFrame (net);
            VisualEvaluationNetwork visualEvaluationNetwork = new VisualEvaluationNetwork (
                                                                                           net,
                                                                                           evaluationPanel.getEditorPanel ());
            for (VisualNode visualNode : visualEvaluationNetwork.getAllNodes ())
            {
                String nodeName = visualNode.getNode ().getName ();
                if (classVariables.contains (nodeName))
                {
                    ((VisualEvaluationNode) visualNode).setSuccessRate (evaluationResult.getSuccessRates ().get (visualNode.getNode ().getVariable ()));
                    visualNode.setExpanded (true);
                }
            }
            evaluationPanel.getEditorPanel ().setVisualNetwork (visualEvaluationNetwork);
            this.setVisible (false);
            // Generate report
            if (chkGenerateReport.isSelected ())
            {
                JFileChooser reportFileChooser = new JFileChooser ();
                File currentDirectory = new File (
                                                  OpenMarkovPreferences.get (OpenMarkovPreferences.LAST_OPEN_DIRECTORY,
                                                                             OpenMarkovPreferences.OPENMARKOV_DIRECTORIES,
                                                                             "."));
                reportFileChooser.setCurrentDirectory (currentDirectory);
                reportFileChooser.addChoosableFileFilter (new FileFilterAll (".xls",
                                                                             "Excel File (.xls)"));
                if (reportFileChooser.showSaveDialog (this) == JFileChooser.APPROVE_OPTION)
                {
                    String filename = reportFileChooser.getSelectedFile ().getAbsolutePath ();
                    EvaluationReportGenerator reportGenerator = new EvaluationReportGenerator ();
                    try
                    {
                        reportGenerator.write (evaluationResult, filename);
                    }
                    catch (IOException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace ();
                    }
                }
            }
        }
    }// GEN-LAST:event_EvaluateButtonActionPerformed

    private void fromOpenMarkovRadioButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_fromOpenMarkovRadioButtonActionPerformed
        if (fromOpenMarkovRadioButton.isSelected ())
        {
            NetworkPanel networkPanel = MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().getCurrentNetworkPanel ();
            net = networkPanel.getProbNet ();
        }
        updateVariableSelection (net);
    }// GEN-LAST:event_fromOpenMarkovRadioButtonActionPerformed

    private void loadModelNetButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_loadModelNetButtonActionPerformed
        netFilePath = requestNetworkFileToOpen ();
        if (netFilePath != null)
        {
            net = loadNet (netFilePath);
            updateVariableSelection (net);
            evaluateButton.setEnabled (caseDatabase != null && net != null);
        }
    }// GEN-LAST:event_loadModelNetButtonActionPerformed

    private void fromFileRadioButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_fromFileRadioButton1ActionPerformed
        if (netFilePath == null)
        {
            netFilePath = requestNetworkFileToOpen ();
            if (netFilePath != null)
            {
                net = loadNet (netFilePath);
                updateVariableSelection (net);
            }
        }
        if (netFilePath != null)
        {
            loadModelNetButton.setEnabled (fromFileRadioButton.isSelected ());
            evaluateButton.setEnabled (caseDatabase != null && net != null);
        }
    }// GEN-LAST:event_fromFileRadioButton1ActionPerformed

    private void loadCaseFileButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_loadCaseFileButtonActionPerformed
        if (dbFileChooser.showOpenDialog (this) == JFileChooser.APPROVE_OPTION)
        {
            loadDatabase ();
            caseFileTextPane.setText (dbFileChooser.getSelectedFile ().getName ());
            evaluateButton.setEnabled (caseDatabase != null && net != null);
        }
    }// GEN-LAST:event_loadCaseFileButtonActionPerformed

    private void unToEvBtnActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_jButton1ActionPerformed
        for (String variable : hiddenVariableList.getSelectedValuesList ())
        {
            hiddenVariables.remove (variable);
            evidenceVariables.add (variable);
        }
        hiddenVariableList.setListData (hiddenVariables);
        evidenceVariableList.setListData (evidenceVariables);
    }// GEN-LAST:event_jButton1ActionPerformed

    private void clToUnBtnActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_jButton2ActionPerformed
        for (String variable : classVariableList.getSelectedValuesList ())
        {
            classVariables.remove (variable);
            hiddenVariables.add (variable);
        }
        classVariableList.setListData (classVariables);
        hiddenVariableList.setListData (hiddenVariables);
    }// GEN-LAST:event_jButton2ActionPerformed

    private void evToUnBtnActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_jButton3ActionPerformed
        for (String variable : evidenceVariableList.getSelectedValuesList ())
        {
            evidenceVariables.remove (variable);
            hiddenVariables.add (variable);
        }
        hiddenVariableList.setListData (hiddenVariables);
        evidenceVariableList.setListData (evidenceVariables);
    }// GEN-LAST:event_jButton3ActionPerformed

    private void unToClBtnActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_jButton4ActionPerformed
        for (String variable : hiddenVariableList.getSelectedValuesList ())
        {
            hiddenVariables.remove (variable);
            classVariables.add (variable);
        }
        hiddenVariableList.setListData (hiddenVariables);
        classVariableList.setListData (classVariables);
    }// GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param path
     * @return whether a file format is supported or not
     */
    private static boolean isSupportedNetFormat (String path)
    {
        return (FilenameUtils.getExtension (path).toLowerCase ().equals ("elv")
                || FilenameUtils.getExtension (path).toLowerCase ().equals ("xml") || FilenameUtils.getExtension (path).toLowerCase ().equals ("pgmx"));
    }

    private void updateVariableSelection (ProbNet net)
    {
        unToEvBtn.setEnabled (true);
        clToUnBtn.setEnabled (true);
        evToUnBtn.setEnabled (true);
        unToClBtn.setEnabled (true);
        hiddenVariables.clear ();
        for (Variable variable : net.getVariables ())
        {
            hiddenVariables.add (variable.getName ());
        }
        hiddenVariableList.setListData (hiddenVariables);
        evidenceVariables.clear ();
        evidenceVariableList.setListData (evidenceVariables);
        classVariables.clear ();
        classVariableList.setListData (classVariables);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JButton      cancelButton;
    private static javax.swing.JFileChooser dbFileChooser;
    private static javax.swing.JTextPane    caseFileTextPane;
    private javax.swing.JCheckBox           chkGenerateReport;
    private javax.swing.JButton             clToUnBtn;
    private javax.swing.JScrollPane         classVarScrollPane;
    private javax.swing.JList<String>       classVariableList;
    private javax.swing.JButton             evToUnBtn;
    private static javax.swing.JButton      evaluateButton;
    private javax.swing.JList<String>       evidenceVariableList;
    private javax.swing.JScrollPane         evidenceVarsScrollPane;
    private static javax.swing.JRadioButton fromFileRadioButton;
    private static javax.swing.JRadioButton fromOpenMarkovRadioButton;
    private javax.swing.JList<String>       hiddenVariableList;
    private javax.swing.JScrollPane         hiddenVarsScrollPane;
    private javax.swing.JLabel              jLabel1;
    private javax.swing.JLabel              jLabel2;
    private javax.swing.JLabel              jLabel3;
    private javax.swing.JLabel              jLabel4;
    private javax.swing.JPanel              jPanel1;
    private javax.swing.JPanel              jPanel2;
    private javax.swing.JPanel              jPanel3;
    private javax.swing.JPanel              jPanel5;
    private javax.swing.JPanel              jPanel8;
    private javax.swing.JScrollPane         jScrollPane2;
    private javax.swing.JScrollPane         jScrollPane5;
    private javax.swing.JButton             loadCaseFileButton;
    private static javax.swing.JButton      loadModelNetButton;
    private javax.swing.ButtonGroup         netButtonGroup;
    private static NetworkFileChooser       netFileChooser;
    private static javax.swing.JTextPane    netFilePathTextPane;
    private javax.swing.JButton             unToClBtn;
    private javax.swing.JButton             unToEvBtn;
    // End of variables declaration//GEN-END:variables
}
