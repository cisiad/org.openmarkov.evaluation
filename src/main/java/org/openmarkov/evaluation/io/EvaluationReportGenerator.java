/*
 * Copyright 2012 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.evaluation.io;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.evaluation.result.EvaluationClassVariable;
import org.openmarkov.evaluation.result.EvaluationResult;
import org.openmarkov.evaluation.result.EvaluationSample;

public class EvaluationReportGenerator
{
    public void write (EvaluationResult result, String filename)
        throws IOException
    {
        // create a workbook
        HSSFWorkbook wb = new HSSFWorkbook ();
        // create a sheet
        HSSFSheet sheet = wb.createSheet ("data");
        // Write header
        // Create a row to put the names of the variables
        HSSFRow headerRow = sheet.createRow (0);
        // Create a row for the sub header
        HSSFRow subHeaderRow = sheet.createRow (1);
        /* Attributes */
        int colIndex = 0;
        
        HSSFFont font = wb.createFont ();
        font.setFontName("Calibri");
        
        HSSFCellStyle headerStyle = wb.createCellStyle();
        headerStyle.setFont (font);
        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND );
        headerStyle.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT () .getIndex());
        headerStyle.setAlignment (HSSFCellStyle.ALIGN_CENTER);
        
        for (Variable variable : result.getEvidenceVariables ())
        {
            // Create a cell and put a value in it.
            HSSFCell cell = headerRow.createCell ( colIndex, HSSFCell.CELL_TYPE_STRING);
            cell.setCellValue (new HSSFRichTextString (variable.getName ()));
            cell.setCellStyle (headerStyle);

            HSSFCell emptyCell = subHeaderRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
            emptyCell.setCellValue (new HSSFRichTextString ());
            emptyCell.setCellStyle (headerStyle);
            
            colIndex++;
        }
        for (Variable variable : result.getClassVariables ())
        {
            for (int i = 0; i < variable.getNumStates (); ++i)
            {
                HSSFCell cell = headerRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
                cell.setCellValue (new HSSFRichTextString (variable.getName ()));
                cell.setCellStyle (headerStyle);

                HSSFCell probabilitycell = subHeaderRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
                probabilitycell.setCellValue (new HSSFRichTextString ("P(" + variable.getStateName (i) + ")"));
                probabilitycell.setCellStyle (headerStyle);
                
                colIndex++;
            }

            // actual
            HSSFCell actualHeaderCell = headerRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
            actualHeaderCell.setCellValue (new HSSFRichTextString (variable.getName ()));
            actualHeaderCell.setCellStyle (headerStyle);

            HSSFCell actualSubHeaderCell = subHeaderRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
            actualSubHeaderCell.setCellValue (new HSSFRichTextString ("actual"));
            actualSubHeaderCell.setCellStyle (headerStyle);
            
            colIndex++;
            
            // mps
            HSSFCell mpsHeaderCell = headerRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
            mpsHeaderCell.setCellValue (new HSSFRichTextString (variable.getName ()));
            mpsHeaderCell.setCellStyle (headerStyle);

            HSSFCell mpsSubHeaderCell = subHeaderRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
            mpsSubHeaderCell.setCellValue (new HSSFRichTextString ("mps"));
            mpsSubHeaderCell.setCellStyle (headerStyle);
            
            colIndex++;
            
            // success
            HSSFCell successHeaderCell = headerRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
            successHeaderCell.setCellValue (new HSSFRichTextString (variable.getName ()));
            successHeaderCell.setCellStyle (headerStyle);

            HSSFCell successSubHeaderCell = subHeaderRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
            successSubHeaderCell.setCellValue (new HSSFRichTextString ("success"));
            successSubHeaderCell.setCellStyle (headerStyle);
            
            colIndex++;            
        }
        
        HSSFCellStyle rowStyle = wb.createCellStyle();
        rowStyle.setFont (font);
        
        // write sample rows
        short rowIndex = 2;
        for(EvaluationSample sample: result.getSamples ())
        {
            // Create a row to put the names of the attributes.
            HSSFRow row = sheet.createRow (rowIndex);
            
            colIndex = 0;
            for (Variable variable : result.getEvidenceVariables ())
            {
                HSSFCell cell = row.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
                cell.setCellValue (new HSSFRichTextString (sample.getEvidenceVariables ().get (variable.getName ())));
                cell.setCellStyle (rowStyle);
                colIndex++;
            }
            for (Variable variable : result.getClassVariables ())
            {
                EvaluationClassVariable evaluationClassVariable = sample.getClassVariables ().get (variable.getName ());
                double[] probabilities = evaluationClassVariable.getStatesProbabilities ();
                for (int i = 0; i < variable.getNumStates (); ++i)
                {
                    HSSFCell cell = row.createCell (colIndex, HSSFCell.CELL_TYPE_NUMERIC);
                    cell.setCellValue (probabilities[i]);
                    cell.setCellStyle (rowStyle);
                    ++colIndex;
                }
                //actual
                HSSFCell actualStateCell = row.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
                actualStateCell.setCellValue (new HSSFRichTextString (evaluationClassVariable.getOriginalState ()));
                actualStateCell.setCellStyle (rowStyle);
                colIndex++;
                //mps
                HSSFCell mpsCell = row.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
                mpsCell.setCellValue (new HSSFRichTextString (evaluationClassVariable.getChosenState ()));
                mpsCell.setCellStyle (rowStyle);
                colIndex++;
                //success
                HSSFCell successCell = row.createCell (colIndex, HSSFCell.CELL_TYPE_BOOLEAN);
                successCell.setCellValue (evaluationClassVariable.isSuccess ());
                successCell.setCellStyle (rowStyle);
                colIndex++;
                
            }

            ++rowIndex;
        }

        //Auto size columns
        for(short i=0; i <= colIndex; ++i)
        {
            sheet.autoSizeColumn (i);
        }
            
        // dump to file
        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream (filename);
        wb.write (fileOut);
        fileOut.close ();
    }
}
