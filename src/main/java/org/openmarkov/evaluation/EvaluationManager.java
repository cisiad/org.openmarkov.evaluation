/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */
package org.openmarkov.evaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.io.database.CaseDatabase;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.evaluation.result.EvaluationResult;
import org.openmarkov.evaluation.result.EvaluationSample;

public class EvaluationManager
{
    public EvaluationResult evaluate (ProbNet net,
                                             CaseDatabase caseDatabase,
                                             Vector<String> classVariableNames,
                                             Vector<String> evidenceVariableNames,
                                             InferenceAlgorithm inferenceAlgorithm)
    {
        List<Variable> classVariables = new ArrayList<Variable> ();
        List<Variable> evidenceVariables = new ArrayList<Variable> ();
        try
        {
            for (String classVariableName : classVariableNames)
            {
                classVariables.add (net.getVariable (classVariableName));
            }
            for (String evidenceVariableName : evidenceVariableNames)
            {
                evidenceVariables.add (net.getVariable (evidenceVariableName));
            }
        }
        catch (NodeNotFoundException e)
        {
            e.printStackTrace ();
        }
        
        EvaluationResult evaluationResult = new EvaluationResult (classVariables, evidenceVariables);    
        int[][] cases = caseDatabase.getCases ();
        List<Variable> dbVariables = caseDatabase.getVariables ();

        for(int i=0; i< cases.length; ++i)
        {
            //build EvidenceCase
            HashMap<Variable, TablePotential> individualProbabilities = null;
            EvaluationSample sample = new EvaluationSample ();
            try
            {
                EvidenceCase evidenceCase = new EvidenceCase ();
                for(String evidenceVariableName : evidenceVariableNames)
                {
                    evidenceCase.addFinding (new Finding (
                                                          net.getVariable (evidenceVariableName),
                                                          cases[i][getIndexOfVariable(dbVariables, evidenceVariableName)]));
                                        
                    sample.addEvidenceVariable (net.getVariable (evidenceVariableName),
                                                cases[i][getIndexOfVariable(dbVariables, evidenceVariableName)]);
                }
                inferenceAlgorithm.setPostResolutionEvidence(evidenceCase);
                individualProbabilities = inferenceAlgorithm.getProbsAndUtilities();
                for(String classVariableName : classVariableNames)
                {
                    Variable classVariable = net.getVariable (classVariableName);
                    TablePotential inferredProbabilities = (TablePotential)individualProbabilities.get (classVariable);
                    int mostProbableState = mostProbableState(inferredProbabilities);
                    int actualState = cases[i][getIndexOfVariable(dbVariables, classVariableName)];
                    sample.addClassVariable (classVariable, inferredProbabilities.values, mostProbableState, actualState);
                }
                
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            evaluationResult.addSample (sample);
            
        }             
        
        return evaluationResult;
    }
    
    private int getIndexOfVariable (List<Variable> dbVariables, String variableName)
    {
        int index = -1;
        int i = 0;
        while(index == -1 && i < dbVariables.size ())
        {
            if(dbVariables.get(i).getName ().equals (variableName))
            {
                index = i;
            }
            ++i;
        }
        return index;
    }

    /**
     * Returns the index of the most probable state
     * @param classVariable
     * @return
     */
    private int mostProbableState (Potential potential)
    {
        double[] values = ((TablePotential)potential).values;
        double maxProbability = 0.0;
        int index = 0;
        for(int i= 0; i< values.length; ++i)
        {
            if(values[i]>maxProbability)
            {
                maxProbability = values[i];
                index = i;
            }
        }
        return index;
    }    
}
